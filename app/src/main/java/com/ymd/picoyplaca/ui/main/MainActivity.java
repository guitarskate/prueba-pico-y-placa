package com.ymd.picoyplaca.ui.main;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.ymd.picoyplaca.R;
import com.ymd.picoyplaca.base.BaseActivity;
import com.ymd.picoyplaca.db.RegistryData;
import com.ymd.picoyplaca.models.Registry;
import com.ymd.picoyplaca.ui.registry.RegistryActivity;
import com.ymd.picoyplaca.utils.ChangeActivity;
import com.ymd.picoyplaca.utils.DataEnum;
import com.ymd.picoyplaca.utils.Utils;
import com.ymd.picoyplaca.widgets.DatePicker;
import com.ymd.picoyplaca.widgets.HourPicker;

import java.util.Calendar;

import butterknife.BindView;

public class MainActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.view_plate)
    View viewPlate;

    @BindView(R.id.txt_plate)
    TextView txtPlate;

    @BindView(R.id.txt_today)
    TextView txtToday;

    @BindView(R.id.txt_day)
    TextView txtDay;

    @BindView(R.id.txt_hour)
    TextView txtHour;

    @BindView(R.id.txt_input_date)
    TextView inputDate;

    @BindView(R.id.txt_input_hour)
    TextView inputHour;

    @BindView(R.id.txt_recidivism)
    TextView recidivism;

    @BindView(R.id.edt_plate)
    EditText edtPlate;

    @BindView(R.id.btn_validate)
    Button btnValidate;

    private RegistryData registryData;
    private String dayName;
    private Calendar calendarHour;

    @Override
    protected int layoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_show_infraction:
                ChangeActivity.animRightLeft(this, RegistryActivity.class, false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initUI(){
        setSupportActionBar(toolbar);

        registryData = new RegistryData(this);
        Registry registry = new Registry();

        HourPicker hourPicker = new HourPicker((hour, calendarHour)->{
            this.calendarHour = calendarHour;
            registry.setHour(hour);

            inputDate.setText(registry.getDate());
            inputHour.setText(registry.getHour());
        });
        DatePicker datePicker = new DatePicker((date, dayName, type)-> {
            this.dayName = dayName;
            registry.setDate(date);
            hourPicker.getHourPicker(MainActivity.this);
        });

        inputDate.setOnClickListener(v->datePicker.getDatePicker(this, 1));
        inputHour.setOnClickListener(v->hourPicker.getHourPicker(this));
        btnValidate.setOnClickListener(v->validatePlate(registry));
    }

    private void validatePlate(Registry registry){
        StringBuffer alphaBefore = new StringBuffer();
        StringBuffer alphaAfter = new StringBuffer();
        StringBuffer num = new StringBuffer();

        String plate = edtPlate.getText().toString();
        boolean passNum = true;

        for (int i=0; i<plate.length(); i++) {
            char letter = plate.charAt(i);
            if(Character.isLetter(letter) && passNum)
                alphaBefore.append(letter);
            else if (Character.isDigit(letter)) {
                num.append(letter);
                passNum = false;
            } else if (Character.isLetter(letter) && !passNum){
                alphaAfter.append(letter);
            }
        }

        System.out.println(String.format(getString(R.string.vehicle_plate),alphaBefore, num, alphaAfter));

        txtPlate.setText(String.format(getString(R.string.vehicle_plate),
                alphaBefore.toString().toUpperCase(),
                num.toString().toUpperCase(),
                alphaAfter.toString().toUpperCase()));

        String lastNumber = num.substring(num.length() -1);

        DataEnum dataEnum = DataEnum.getByDays(Integer.parseInt(lastNumber));
        txtDay.setText(dataEnum.getDay());
        txtToday.setText(String.format(getString(R.string.pico_y_placa_hoy), dataEnum.getPlaca()[0], dataEnum.getPlaca()[1]));

        if(Utils.validateDays(dataEnum.getDay(), dayName)){
            if(Utils.validateHour(calendarHour)) {
                changeColor(getResources().getColor(R.color.colorAccent));
                registry.setInfraction(1);
                recidivism.setText(String.format(getString(R.string.recidivism), registryData.countRecidivism(plate)));
            }
            else {
                changeColor(getResources().getColor(R.color.backgroundAlert));
                registry.setInfraction(0);
            }
        } else {
            changeColor(getResources().getColor(R.color.backgroundSuccess));
            registry.setInfraction(0);
        }

        registry.setPlate(plate);
        registryData.insertRegister(registry);
    }

    /**
     * Cambia el color del toolbar y de la placa para facilitar
     * la identificacion de las faltas
     *
     * @param color - color de la falta
     */
    private void changeColor(int color){
        toolbar.setBackgroundColor(color);
        viewPlate.setBackgroundColor(color);
    }
}

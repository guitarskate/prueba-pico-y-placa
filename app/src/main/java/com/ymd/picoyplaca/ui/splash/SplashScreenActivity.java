package com.ymd.picoyplaca.ui.splash;

import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;

import com.ymd.picoyplaca.R;
import com.ymd.picoyplaca.base.BaseActivity;
import com.ymd.picoyplaca.ui.main.MainActivity;
import com.ymd.picoyplaca.utils.ChangeActivity;
import com.ymd.picoyplaca.utils.Constants;

public class SplashScreenActivity extends BaseActivity {

    @Override
    protected int layoutRes() {
        return R.layout.activity_splash_screen;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new Handler().postDelayed(() -> ChangeActivity.animRightLeft(SplashScreenActivity.this, MainActivity.class),
                Constants.SPLASH_SCREEN_DELAY);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return false;
    }
}

package com.ymd.picoyplaca.ui.registry;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.ymd.picoyplaca.R;
import com.ymd.picoyplaca.adapters.ItemRegistryAdapter;
import com.ymd.picoyplaca.base.BaseActivity;
import com.ymd.picoyplaca.db.RegistryData;
import com.ymd.picoyplaca.models.Registry;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;

public class RegistryActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recycler_registry)
    RecyclerView recyclerRegistry;

    private RegistryData registryData;
    private List<Registry> registryList;

    @Override
    protected int layoutRes() {
        return R.layout.activity_registry;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initUI();
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return false;
    }

    private void initUI(){
        registryData = new RegistryData(this);
        registryList = new ArrayList<>();
        initToolbar();
        getRegisterFromDB();
    }

    /**
     * Consulta y muestra los registros almacenados
     */
    private void getRegisterFromDB(){
        registryList.addAll(registryData.selectAllRegister());

        GridLayoutManager gridManagerFeatures = new GridLayoutManager(this, 1);
        gridManagerFeatures.setOrientation(RecyclerView.VERTICAL);

        recyclerRegistry.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerRegistry.setAdapter(new ItemRegistryAdapter(this, registryList));
        recyclerRegistry.setLayoutManager(gridManagerFeatures);
        recyclerRegistry.setNestedScrollingEnabled(false);
    }

}

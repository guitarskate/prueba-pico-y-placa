package com.ymd.picoyplaca.db;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ymd.picoyplaca.models.Registry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yulian Martinez on 05/09/19.
 * @author Yulian Martinez
 *
 * Clase encargada de crear la base de datos local con sus respectivas tablas y consultas
 */
@SuppressLint("Recycle")
public class RegistryData extends SQLiteOpenHelper {

    private static final String TB_USERS = "DBRegistryData";

    public RegistryData(Context context) {
        super(context, "RegistryData", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE IF NOT EXISTS " + TB_USERS +" (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "plate VARCHAR(150), " +
                "date VARCHAR(150), " +
                "hour VARCHAR(150), " +
                "infraction INTEGER" +
                ")");
    }

    /**
     * Agrega un registro a la base de datos
     * @param registry - Clase Registry con todos los datos que se van a almacenar
     */
    public void insertRegister(Registry registry){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues insertUser = new ContentValues();
        insertUser.put("plate", registry.getPlate());
        insertUser.put("date", registry.getDate());
        insertUser.put("hour", registry.getHour());
        insertUser.put("infraction", registry.getInfraction());

        db.insert(TB_USERS, null, insertUser);
    }

    /**
     * Realiza la busqueda de todos los registro con infraccion de una placa determinada
     * @param plate - placa de los registros que se desea buscar
     * @return - Lista de Registers con todos los datos encontrados en la busqueda
     */
    public List<Registry> selectRegister(int plate){
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("SELECT plate, date, hour, infraction FROM " + TB_USERS + " WHERE plate = '"+plate+"' AND infraction = '1'", null);

        List<Registry> registryList = new ArrayList<>();
        Registry registry = new Registry();
        while (c.moveToNext()){
            registry.setPlate(c.getString(0));
            registry.setDate(c.getString(1));
            registry.setHour(c.getString(2));
            registry.setInfraction(c.getInt(3));

            registryList.add(registry);
        }

        return registryList;
    }

    /**
     * Realiza la busqueda de todos los registro
     *
     * @return - Lista de Registers con todos los datos encontrados en la busqueda
     */
    public List<Registry> selectAllRegister(){
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("SELECT plate, date, hour, infraction FROM " + TB_USERS, null);

        List<Registry> registryList = new ArrayList<>();
        while (c.moveToNext()){
            Registry registry = new Registry();
            registry.setPlate(c.getString(0));
            registry.setDate(c.getString(1));
            registry.setHour(c.getString(2));
            registry.setInfraction(c.getInt(3));

            registryList.add(registry);
        }

        return registryList;
    }

    public int countRecidivism(String plate){
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("SELECT count(*) FROM " + TB_USERS + " WHERE plate = '"+plate+"' AND infraction = '1'", null);

        while (c.moveToNext()){
            return  c.getInt(0);
        }

        return 0;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

package com.ymd.picoyplaca.models;

public class Registry {

    private int id;
    private String plate;
    private String date;
    private String hour;
    private int infraction;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public int getInfraction() {
        return infraction;
    }

    public void setInfraction(int infraction) {
        this.infraction = infraction;
    }
}

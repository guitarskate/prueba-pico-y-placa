package com.ymd.picoyplaca.widgets;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.content.Context;

import com.ymd.picoyplaca.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class HourPicker {

    private int HOUR_OF_DAY = 0;
    private int MINUTE = 0;
    private OnGetHourListener listener;

    public HourPicker(OnGetHourListener listener) {
        this.listener = listener;
    }

    public void getHourPicker(final Context context){
        final Calendar calendar = Calendar.getInstance();

        TimePickerDialog getHour = new TimePickerDialog(context, R.style.ThemeDatePicker, (timePicker, hour, minute) -> {
            HOUR_OF_DAY = hour;
            MINUTE = minute;

            @SuppressLint("SimpleDateFormat")
            DateFormat input = new SimpleDateFormat("hh:mm", Locale.US);
            DateFormat formatter = new SimpleDateFormat("h:mm a", Locale.US);

            calendar.set(Calendar.HOUR_OF_DAY, HOUR_OF_DAY);
            calendar.set(Calendar.MINUTE, MINUTE);
            calendar.set(Calendar.SECOND, 0);

            Date date = null;
            try {
                date = input.parse(String.format(context.getString(R.string.hour_format), HOUR_OF_DAY, MINUTE));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            //Muestro la fecha con el formato deseado
            listener.onGetHour(formatter.format(date), calendar);
        }, HOUR_OF_DAY, MINUTE,false);
        //Muestro el widget
        getHour.show();
    }

    public interface OnGetHourListener {
        void onGetHour(String hour, Calendar calendar);
    }
}

package com.ymd.picoyplaca.widgets;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;

import com.ymd.picoyplaca.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@SuppressWarnings("deprecation")
@SuppressLint("SimpleDateFormat")
public class DatePicker {

    private int MONTH = 0;
    private int DAY = 0;
    private int YEAR = 0;
    private OnGetDateListener listener;

    public DatePicker(OnGetDateListener listener) {
        this.listener = listener;
    }

    private void getDatePicker(final Context context, int type, boolean minDate){
        final Calendar calendar = Calendar.getInstance();
        if(this.MONTH == 0 && this.DAY == 0 && this.YEAR == 0){
            this.MONTH = calendar.get(Calendar.MONTH);
            this.DAY = calendar.get(Calendar.DAY_OF_MONTH);
        }

        //Estos valores deben ir en ese orden, de lo contrario no mostrara la fecha actual
        DatePickerDialog getDate = new DatePickerDialog(context, R.style.ThemeDatePicker, (view, currentYear, currentMonth, currentDay) -> {
            this.MONTH = currentMonth;
            this.DAY = currentDay;
            this.YEAR = currentYear;

            //Esta variable lo que realiza es aumentar en uno el month ya que comienza desde 0 = enero
            final int mesActual = currentMonth + 1;
            //Formateo el día obtenido: antepone el 0 si son menores de 10
            String diaFormateado = (currentDay < 10)? "0" + currentDay : String.valueOf(currentDay);
            //Formateo el month obtenido: antepone el 0 si son menores de 10
            String mesFormateado = (mesActual < 10)? "0" + mesActual : String.valueOf(mesActual);

            SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
            Date date = new Date(this.YEAR, this.MONTH, this.DAY-1);
            String dayOfWeek = simpledateformat.format(date);

            //Muestro la fecha con el formato deseado
            listener.onGetDate(String.format(context.getResources().getString(R.string.date_format),currentYear,mesFormateado,diaFormateado), dayOfWeek, type);
        }, this.YEAR, this.MONTH, this.DAY);
        if (minDate)
            getDate.getDatePicker().setMinDate(calendar.getTimeInMillis());
        //Muestro el widget
        getDate.show();
    }

    public void getDatePicker(final Context context, int type, int year){
        this.YEAR = year;
        getDatePicker(context, type, false);
    }

    public void getDatePicker(final Context context, int type){
        final Calendar calendar = Calendar.getInstance();
        this.YEAR = calendar.get(Calendar.YEAR);
        getDatePicker(context, type, true);
    }

    public interface OnGetDateListener {
        void onGetDate(String date, String dayName, int type);
    }
}

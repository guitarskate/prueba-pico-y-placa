package com.ymd.picoyplaca.utils;

public enum DataEnum {

    LUNES(new int[]{1, 2}, "Lunes"),
    MARTES(new int[]{3, 4}, "Martes"),
    MIERCOLES(new int[]{5, 6}, "Miercoles"),
    JUEVES(new int[]{7, 8}, "Jueves"),
    VIERNES(new int[]{9, 0}, "Viernes");

    private int[] placa;
    private String day;

    DataEnum(int[] placa, String day) {
        this.placa = placa;
        this.day = day;
    }

    public int[] getPlaca() {
        return this.placa;
    }
    public String getDay() {
        return this.day;
    }

    public static DataEnum getByDays(int day) {
        for (DataEnum dataEnum : DataEnum.values()) {
            for (int element : dataEnum.placa) {
                if (element == day) {
                    return dataEnum;
                }
            }
        }
        return null;
    }
}

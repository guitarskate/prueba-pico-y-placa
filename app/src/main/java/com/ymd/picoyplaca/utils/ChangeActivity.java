package com.ymd.picoyplaca.utils;

import android.content.Intent;

import androidx.fragment.app.FragmentActivity;

import com.ymd.picoyplaca.R;

@SuppressWarnings({"WeakerAccess", "unused"})
public class ChangeActivity {

    private static final boolean CLOSE_DEFAULT = true;
    private static final boolean RIGHT_DEFAULT = true;
    private static final boolean LEFT_DEFAULT = false;

    /**
     * funcion que cambia de actividad y realiza una animacion a la actividad
     * @param context - contexto
     * @param intent - componente con datos para pasar a la siguiente actividad
     * @param close - decide si cerrar la actividad actual o no
     * @param rightOrLeft - true si es animacion (Right To Left) y false si es animacion (Left To Right)
     */
    private static void animActivity(FragmentActivity context, Intent intent, boolean close, boolean rightOrLeft){
        context.startActivity(intent);
        if(rightOrLeft)
            context.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        else
            context.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        if(close)
            context.finish();
    }

    public static void animRightLeft(FragmentActivity context, Intent intent, boolean close){
        animActivity(context, intent, close, RIGHT_DEFAULT);
    }

    public static void animLeftRight(FragmentActivity context, Intent intent, boolean close){
        animActivity(context, intent, close, LEFT_DEFAULT);
    }

    public static void animRightLeft(FragmentActivity context, Class activity, boolean close){
        animRightLeft(context, new Intent(context, activity), close);
    }

    public static void animRightLeft(FragmentActivity context, Intent intent){
        animRightLeft(context, intent, CLOSE_DEFAULT);
    }

    public static void animRightLeft(FragmentActivity context, Class activity){
        animRightLeft(context, new Intent(context, activity), CLOSE_DEFAULT);
    }

    public static void animLeftRight(FragmentActivity context, Class activity, boolean close){
        animLeftRight(context, new Intent(context, activity), close);
    }

    public static void animLeftRight(FragmentActivity context, Intent intent){
        animLeftRight(context, intent, CLOSE_DEFAULT);
    }

    public static void animLeftRight(FragmentActivity context, Class activity){
        animLeftRight(context, new Intent(context, activity), CLOSE_DEFAULT);
    }

}

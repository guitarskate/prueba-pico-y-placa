package com.ymd.picoyplaca.utils;

import java.util.Calendar;

public class Utils {

    public static boolean validateDays(String dayEnum, String dayCalendar){
        dayEnum = dayEnum.toLowerCase();
        dayCalendar = dayCalendar.toLowerCase();
        return dayEnum.equals(dayCalendar);
    }

    public static boolean validateHour(Calendar now) {
        Calendar one = Calendar.getInstance();
        one.set(Calendar.HOUR_OF_DAY, 7);
        one.set(Calendar.MINUTE, 0);
        one.set(Calendar.SECOND, 0);

        Calendar two = Calendar.getInstance();
        two.set(Calendar.HOUR_OF_DAY, 9);
        two.set(Calendar.MINUTE, 30);
        two.set(Calendar.SECOND, 0);

        Calendar three = Calendar.getInstance();
        three.set(Calendar.HOUR_OF_DAY, 16);
        three.set(Calendar.MINUTE, 0);
        three.set(Calendar.SECOND, 0);

        Calendar four = Calendar.getInstance();
        four.set(Calendar.HOUR_OF_DAY, 19);
        four.set(Calendar.MINUTE, 30);
        four.set(Calendar.SECOND, 0);


        if(now.getTime().after(one.getTime()) && now.getTime().before(two.getTime())) {
            return true;
        }

        if(now.getTime().after(three.getTime()) && now.getTime().before(four.getTime())) {
            return true;
        }

        return false;
    }

}

package com.ymd.picoyplaca.adapters;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ymd.picoyplaca.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yulian Martinez on 19/03/19.
 * @author Yulian Martinez
 */
public class RegistryViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.txt_item_plate)
    protected TextView plate;

    @BindView(R.id.txt_item_date)
    protected TextView date;

    @BindView(R.id.txt_item_hour)
    protected TextView hour;

    @BindView(R.id.txt_item_infraction)
    protected TextView infraction;

    public RegistryViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }
}
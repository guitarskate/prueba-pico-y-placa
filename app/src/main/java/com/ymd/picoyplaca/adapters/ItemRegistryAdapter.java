package com.ymd.picoyplaca.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ymd.picoyplaca.R;
import com.ymd.picoyplaca.models.Registry;

import java.util.List;

/**
 * Created by Yulian Martinez on 19/03/19.
 * @author Yulian Martinez
 */
public class ItemRegistryAdapter extends RecyclerView.Adapter<RegistryViewHolder> {

    private List<Registry> registries;
    private Context context;

    public ItemRegistryAdapter(Context context, List<Registry> posts) {
        this.context = context;
        this.registries = posts;
    }

    @NonNull
    @Override
    @SuppressLint("InflateParams")
    public RegistryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_registry, null);

        return new RegistryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RegistryViewHolder postViewHolder, int i) {
        final Registry itemPost = registries.get(i);

        postViewHolder.plate.setText(itemPost.getPlate());
        postViewHolder.date.setText(itemPost.getDate());
        postViewHolder.hour.setText(itemPost.getHour());
        if(itemPost.getInfraction() == 1){
            postViewHolder.infraction.setText("Con infraccion");
            postViewHolder.infraction.setTextColor(context.getResources().getColor(R.color.colorAccent));
        } else {
            postViewHolder.infraction.setText("Sin Infraccion");
            postViewHolder.infraction.setTextColor(context.getResources().getColor(R.color.backgroundSuccess));
        }
    }

    @Override
    public int getItemCount() {
        return (null != registries ? registries.size() : 0);
    }
}
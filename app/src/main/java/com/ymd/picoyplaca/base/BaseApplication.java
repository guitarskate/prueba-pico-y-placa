package com.ymd.picoyplaca.base;

import android.app.Application;
import android.content.Context;

import com.squareup.picasso.Picasso;
import com.ymd.picoyplaca.BuildConfig;


public class BaseApplication extends Application {

    private static BaseApplication instance;

    public void onCreate() {
        super.onCreate();
        instance = this;

        if (BuildConfig.DEBUG) {
            Picasso.get().setIndicatorsEnabled(true);
        }
    }

    public static Context getAppContext() {
        return instance;
    }
}